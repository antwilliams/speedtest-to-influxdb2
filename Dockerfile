FROM python:3.8-slim-buster
LABEL maintainer="Marty Oehme" \
    description="Original by Aiden Gilmartin & Breadlysm."

ENV DEBIAN_FRONTEND=noninteractive

RUN true &&\
\
# Install dependencies
apt-get update && \
apt-get -q -y install --no-install-recommends apt-utils gnupg1 apt-transport-https dirmngr && \
\
# Add repos
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 379CE192D401AB61 && \
echo "deb https://ookla.bintray.com/debian buster main" > /etc/apt/sources.list.d/speedtest.list && \
apt-get update && \
apt-get -q -y install --no-install-recommends speedtest && \
\
# Install Python packages
pip3 install pythonping influxdb-client && \
\
# Clean up
apt-get -q -y autoremove && apt-get -q -y clean && \
rm -rf /var/lib/apt/lists/*

# Final setup & execution
WORKDIR /app
COPY entrypoint.sh main.py /app
ENTRYPOINT ["/bin/sh", "/app/entrypoint.sh"]
CMD ["main.py"]
