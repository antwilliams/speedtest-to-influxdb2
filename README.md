# Speedtest to InfluxDBv2
---

This is a Python script that will continuously run the official Speedtest CLI application by Ookla, takes input from environment variables, formats data and writes it to an InfluxDB v2 database.

This script will allow you to measure your internet connections speed and consistency over time. It uses env variables as configuration. It's as easy to use as telling your Docker server a 1 line command and you'll be set. Using Grafana you can start exploring this data easily.

Breadlysm built a grafana dashboard for this data that can be found at https://grafana.com/grafana/dashboards/13053. Additionally, an extended version of that dash by other contributors can be found [here](https://github.com/breadlysm/speedtest-to-influxdb/blob/master/GrafanaDash-SpeedTests.json). Use `GrafanaDash-SpeedTests.json` to import that dash into Grafana, though you will have to adapt it a lot to work with the newer InfluxDB version.

![OriginalDash](https://user-images.githubusercontent.com/3665468/116284820-8038ca00-a75b-11eb-9b30-4a9d26434f8d.png)
![Variant](https://user-images.githubusercontent.com/945191/105287048-46f52a80-5b6c-11eb-9e57-038d63b67efb.png)

There are some added features to allow some additional details that Ookla provides as tags on your data. Some examples are your current ISP, the interface being used, the server who hosted the test. Over time, you could identify if some servers are performing better than others.

## Differences to InfluxDB v1 program:

* Slightly different environment variables to set up InfluxDB connection
* Speeds will be returned in bytes without transforming into MBit/s, just like the speedtest does
* The measure duplication of `speeds>bandwith_up/down` and `download>bandwith` and `upload>bandwith` has been removed, leaving only the `speeds` fields. Same thing with `ping` and `speeds>latency/jitter` duplication.
* Setting a ping/speedtest interval of 0 will turn the respective test off
* By default only failed tests will be printed to stdout/stderr, this can be controlled through `SPEEDTEST_DEBUG`

## Configuring the script

The InfluxDB connection settings are controlled by environment variables.

The variables available are:
- NAMESPACE = default - None
- INFLUX_DB_ADDRESS = default - influxdb:8086
- INFLUX_DB_TOKEN = no default, *required* for any operation
- INFLUX_DB_ORG = default - speed-org
- INFLUX_DB_BUCKET = default - speedtests
- INFLUX_DB_VERIFY_SSL = default - True
- INFLUX_DB_TAGS = default - None * See below for options, `'*'` widcard for all *
- SPEEDTEST_INTERVAL = default - 5 (minutes)
- SPEEDTEST_SERVER_ID = default - {blank} * id from https://c.speedtest.net/speedtest-servers-static.php *
- PING_INTERVAL = default - 5 (seconds)
- PING_TARGETS = default - 1.1.1.1, 8.8.8.8 (csv of hosts to ping)
- SPEEDTEST_DEBUG = default - False

### Variable Notes
- Intervals are in minutes. *Script will convert it to seconds.*
- If any variables are not needed, don't declare them. Functions will operate with or without most variables.
- Tags should be input without quotes. *INFLUX_DB_TAGS = isp, interface, external_ip, server_name, speedtest_url*
- NAMESPACE is used to collect data from multiple instances of the container into one database and select which you wish to view in Grafana. i.e. Breadlysm has one monitoring their Starlink, the other their TELUS connection.

### Tag Options
The Ookla speedtest app provides a nice set of data beyond the upload and download speed. The list is below.

| Tag Name 	| Description 	|
|-	|-	|
| isp 	| Your connections ISP 	|
| interface 	| Your devices connection interface 	|
| internal_ip 	| Your container or devices IP address 	|
| interface_mac 	| Mac address of your devices interface 	|
| vpn_enabled 	| Determines if VPN is enabled or not? I wasn't sure what this represented 	|
| external_ip 	| Your devices external IP address 	|
| server_id 	| The Speedtest ID of the server that  was used for testing 	|
| server_name 	| Name of the Speedtest server used  for testing 	|
| server_country 	| Country where the Speedtest server  resides 	|
| server_location | Location where the Speedtest server  resides  |
| server_host 	| Hostname of the Speedtest server 	|
| server_port 	| Port used by the Speedtest server 	|
| server_ip 	| Speedtest server's IP address 	|
| speedtest_id 	| ID of the speedtest results. Can be  used on their site to see results 	|
| speedtest_url 	| Link to the testing results. It provides your results as it would if you tested on their site.  	|

### Additional Notes
Be aware that this script will automatically accept the license and GDPR statement so that it can run non-interactively. Make sure you agree with them before running.

## Running the Script

### Ideal option, run as a Docker container.

1. Build the container.

    `docker build -t speedtest-to-influxdbv2 ./`

2. Run the container.
    ```
     docker run -d -t --name speedflux \
    -e 'NAMESPACE'='None' \
    -e 'INFLUX_DB_ADDRESS'='influxdb:8086' \
    -e 'INFLUX_DB_ORG'='my-awesome-org' \
    -e 'INFLUX_DB_TOKEN'='1NCR3D1BLY-L0NG-T0K3N-ST71NG' \
    -e 'INFLUX_DB_BUCKET'='speedtests' \
    -e 'SPEEDTEST_INTERVAL'='5' \
    -e 'SPEEDTEST_FAIL_INTERVAL'='5'  \
    -e 'SPEEDTEST_SERVER_ID'='12746' \
    speedtest-to-influxdbv2
    ```

As of now, the container is also reachable through this repository's container registry:

`docker run <FLAGS> registry.gitlab.com/marty-oehme/speedtest-to-influxdb2:master`


---

This script looks to have been originally written by https://github.com/aidengilmartin/speedtest-to-influxdb/blob/master/main.py and I forked it from https://github.com/breadlysm/speedtest-to-influxdb. They did the hard work, I've continued to modify it though to fit my needs.
